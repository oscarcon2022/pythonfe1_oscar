#Oscar Contreras
#looping project
#Carlow university
#pyramid activity :


pyramid_size = int(input("Enter the pyramid size: "))
even_char = input("Enter the character for even cycles: ")
odd_char = input("Enter the character for odd cycles: ")
space_count = int(input("Enter the number of spaces between characters: "))
duplicate_count = int(input("Enter the duplicate count: "))

# Loop to generate the pyramid art
for row in range(1, pyramid_size + 1):
    if row % 2 == 0:
        char = even_char
    else:
        char = odd_char

    line = (char * duplicate_count + ' ' * space_count) * row
    print(line)
