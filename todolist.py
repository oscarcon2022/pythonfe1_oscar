#Oscar Contreras
#Carlow University
#to do list project



List = [
  "1.Go to the gym",
  "2.Do my school work",
  "3.Pick my brother up from school",
  "4.Go to the park",
  "5.Go to the mall"
]

while True: 
  print("Welcome to your to-do list")
  print("Pick what you want to do with your list:")
  print("1. View your list")
  print("2. Remove from your list")
  print("3. Add to your list")
  print("4. Exit")
  user = input("Enter your choice: ")

  if user == "1":
    for item in List:
      print(item)

  elif user == "2":
    remove_item = input("enter the numbered item you want to remove from the list: ")
    if remove_item in List:
      List.remove(remove_item)
      print(List)
    else: 
      print("type a valid number: ")
      
    
  elif user == "3":
    new_item = input("Enter a new item to add to your list: ")
    List.append(new_item)
    print(List)
    

  elif user == "4":
    print("Goodbye!")
    break

  else:
   print("Invalid choice. Please choose a valid option (1, 2, 3, or 4).")
