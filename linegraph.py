#Oscar Contreras
#Carlow University
#December 4th, 2023
#Final Project
#LINE GRAPH SECTION


import matplotlib.pyplot as plt

# Sheet 16:F8
#graph number 1. LINE GRAPH.


years=[ 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]

uk_studio_backed = [22.6, 22.8, 15.5, 10.7, 34.2, 28.5, 27.8, 32.9, 32.3, 35.9]

uk_independent = [13.1, 9.3, 6.6, 16.1, 10.5, 7.4, 9.6, 13.2, 14.2, 4.9]

# Plotting

plt.figure(figsize=(9,7))

plt.plot(years,uk_studio_backed, label = "A film financed and controlled by a major US studio", color = "purple", linestyle = "dashdot", linewidth = 3)
plt.plot(years, uk_independent,color = "grey", label = "UK independent" )

plt.title("Figure 8 UK films’ share of the UK theatrical market, 2012-2021")
plt.xlabel("Years")
plt.ylabel('Revenue (in billion dollars)')



plt.legend()


plt.show()

