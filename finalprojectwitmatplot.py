#Oscar Contreras
#Carlow University
#December 4th, 2023
#Final Project

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Sheet 8: T4

years=[ 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]

boxoffice=[1099, 1083, 1063, 1242, 1228, 1279, 1282, 1254, 307, 545]



plt.plot(years,boxoffice,label= "Box Office gross", color = "red", linestyle = "solid", linewidth = 3)

plt.title("Table 4 UK box office trends , 2012-2021")
plt.xlabel("Years")
plt.ylabel("Box office gross (£ million)")



#line number 2



plt.legend()

plt.show()

#LINE 2

