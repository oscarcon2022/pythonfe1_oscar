#Oscar Contreras
#pie graph

import pandas as pd
from matplotlib import pyplot as plt


years = [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]

uk_studio_backed = [22.6, 22.8, 15.5, 10.7, 34.2, 28.5, 27.8, 32.9, 32.3, 35.9]

uk_independent = [13.1, 9.3, 6.6, 16.1, 10.5, 7.4, 9.6, 13.2, 14.2, 4.9]


df = pd.DataFrame({
    'Year': years,
    'UK Studio Backed': uk_studio_backed,
    'UK Independent': uk_independent
})


plt.figure(figsize=(9, 6))

labels = [str(year) for year in years]
plt.pie(df['UK Studio Backed'], labels=labels, autopct='%1.1f%%', startangle=90)

plt.title("UK Studio Backed Films' Share of the UK Theatrical Market (2012-2021)")
plt.show()



