#Oscar Contreras
#Final project
#BAR CHART

import matplotlib.pyplot as plt


years=[ 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]

uk_studio_backed = [22.6, 22.8, 15.5, 10.7, 34.2, 28.5, 27.8, 32.9, 32.3, 35.9]

uk_independent = [13.1, 9.3, 6.6, 16.1, 10.5, 7.4, 9.6, 13.2, 14.2, 4.9]

plt.figure(figsize=(9,6))

bars1 = plt.bar(years,uk_studio_backed, label = "A film financed and controlled by a major US studio, qualifying as British under the cultural test for film.", color = "yellow")
bars2 = plt.bar(years, uk_independent, color = "black", label = "UK independent")



bars1[0].set_hatch('/')
bars1[1].set_hatch('/')
bars1[2].set_hatch('/')
bars1[3].set_hatch('/')
bars1[4].set_hatch('/')
bars1[5].set_hatch('/')
bars1[6].set_hatch('/')
bars1[7].set_hatch('/')
bars1[8].set_hatch('/')
bars1[9].set_hatch("/")

plt.title("Figure 8 UK films’ share of the UK theatrical market, 2012-2021")
plt.xlabel("Years")
plt.ylabel('Revenue (in billion dollars)')



plt.legend()

plt.show()













