#Oscar Contreras
#encryption project
#November 11th 2023
alphabet = {
  "a": "z",
  "b": "y",
  "c": "x",
  "d": "w",
  "e": "v",
  "f": "u",
  "g": "t",
  "h": "s",
  "i": "r",
  "j": "q",
  "k": "p",
  "l": "o",
  "m": "n",
  "n": "m",
  "o": "l",
  "p": "k",
  "q": "j",
  "r": "i",
  "s": "h",
  "t": "g",
  "u": "f",
  "v": "e",
  "w": "e",
  "x": "d",
  "y": "c",
  "z": "a",
  "9": "0",
  "0": "2",
  "1": "3"
}

while True:
  user = input("Enter your message here (type 'exit' to end): ")

  if user.lower() == 'exit':
      break

  new_messages_encrypt = []

  for letter in user:
      if letter.lower() in alphabet:
          new_messages_encrypt.append(alphabet[letter.lower()])
      else:
          new_messages_encrypt.append(letter)

  print("Your new message is:", ''.join(new_messages_encrypt))



