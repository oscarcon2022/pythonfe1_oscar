#Oscar Contreras
#Carlow University
#Guessing game
import random as rand

print("Welcome to the GPA Guessing Game!")

# Ask the user for their GPA and proximity range
user_gpa = float(input("Please think of your GPA and enter it: "))
prox_gpa = float(input("How close should the computer's guess be to your GPA? Enter a range: "))

# Calculate the range boundaries
bottom_range = user_gpa - prox_gpa
top_range = user_gpa + prox_gpa

counter = 1
guess = 0

while guess < bottom_range or guess > top_range:
    guess = rand.uniform(0, 4.0)  # Assuming GPA is between 0 and 4.0
    print("Guess #", counter, ": ", guess)
    counter += 1

print("*****************************************************")
print("SUCCESS! The computer guessed", guess, "on Guess #", counter, ".")
closeness = abs(user_gpa - guess)
print("The computer was", closeness, "away from guessing your GPA.")





