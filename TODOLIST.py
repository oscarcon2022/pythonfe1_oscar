# Oscar Contreras
# Carlow University
# To-do list project

# Use a list to store your to-do items without numbers
list = [
    "1.Go to the gym",
    "2.Do my school work",
    "3.Pick my brother up from school",
    "4.Go to the park",
    "5.Go to the mall"
]

while True:
    print("Welcome to your to-do list")
    print("Pick what you want to do with your list:")
    print("1. View your list")
    print("2.Remove from your list")
    print("3.Add to your list")
    print("4.Exit")
    user = input("Enter your choice: ")

    if user == "1":
        for item in list:
         print(item)

    elif user == "2":
        remove_list = int(input("Enter the number of the item you want to remove from the list: "))
        if remove_list == 1:
            del list [0]
            print("Sucess you have removed going to the gym from your list")

        elif remove_list ==2 :
            del list [1]
            print("Sucess, you have removed doing your school work from your list")

        elif remove_list == 3:
           del list[2]
           print("Sucess! you have removed picking my brother up from school from the list")

        elif remove_list == 4:
            del list [3]
            print("Sucess! you have removed Going to the park from your list")


        elif remove_list == 5:
            del list [4]
            print("Sucess!you have removed going to the mall from your list!")

        else:
            print("Invalid number. Please enter a valid item number.")

    elif user == "3":
        new_item = input("Enter a new item to add to your list: ")
        list.append(new_item)
        print("Sucess! you have added," + new_item, "to your list!")

    elif user == "4":
        print("Goodbye!")
        break

    else:
        print("Invalid choice. Please choose a valid option (1, 2, 3, or 4).")






















