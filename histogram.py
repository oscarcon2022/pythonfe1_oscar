#Oscar Contreras
#Final Project
#Histogram graph
import pandas as pd
import matplotlib.pyplot as plt

# Data
data = {
    'Year': [2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021],
    'UK studio-backed': [22.6, 22.8, 15.5, 10.7, 34.2, 28.5, 27.8, 32.9, 32.3, 35.9],
    'UK independent': [13.1, 9.3, 6.6, 16.1, 10.5, 7.4, 9.6, 13.2, 14.2, 4.9]
}


df = pd.DataFrame(data)


df.plot(x='Year', y=['UK studio-backed', 'UK independent'], kind='bar', stacked=True)

plt.xlabel('Year')

plt.ylabel('Market share %')

plt.title('UK films’ share of the UK theatrical market, 2012-2021')

plt.legend(['UK studio-backed', 'UK independent'])

plt.show()









