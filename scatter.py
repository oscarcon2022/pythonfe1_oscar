#Oscar Contreras
#Final project
#scatter plot

from matplotlib import pyplot as plt



years=[ 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021]

uk_studio_backed = [22.6, 22.8, 15.5, 10.7, 34.2, 28.5, 27.8, 32.9, 32.3, 35.9]

uk_independent = [13.1, 9.3, 6.6, 16.1, 10.5, 7.4, 9.6, 13.2, 14.2, 4.9]

plt.figure(figsize= (9,4))

sizes = [209, 486 , 381, 255, 191, 315, 185, 228, 175 , 538]

#s = size, C = color,
plt.scatter(years,uk_studio_backed, s = sizes, c = "orange", edgecolor = "black", linewidth = 1 , alpha = 0.75, label = "A film financed and controlled by a major US studio" )

plt.scatter(years,uk_independent, s = sizes, c = "black", edgecolor ="red" , label = "UK independent")




plt.title("Figure 8 UK films’ share of the UK theatrical market, 2012-2021")
plt.xlabel("Years")
plt.ylabel('Revenue (in billion dollars)')



plt.legend()

plt.show()